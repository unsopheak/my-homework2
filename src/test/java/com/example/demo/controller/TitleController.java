package com.example.demo.controller;

import com.example.demo.service.TitleServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller

public class TitleController {
    private TitleServiceImp titleService;

    @Autowired
    public TitleController(TitleServiceImp titleService){
        this.titleService=titleService;
    }
    @GetMapping("")
    public String viewIndex(ModelMap modelMap){
        modelMap.addAttribute("titleList",titleService.getAll());
        return "index";
    }
}
