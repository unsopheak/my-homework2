package com.example.demo.repository;

import com.example.demo.model.Title;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TitleRepository {
    List<Title> titleList = new ArrayList<>();

    public TitleRepository(){
        Faker faker = new Faker();
        for (int i=1;i<=10;i++){
            Title title =new Title();
            title.setId(i);
            title.setTitle(faker.name().fullName());
            title.setDes("Chuck Norris burst the dot com bubble.");

            titleList.add(title);


        }
    }

    public List<Title> getAllTitle(){
        return titleList;
    }

}
